package seartipy.starter.suites

import org.scalatest.FunSuite

import seartipy.starter.Combinatorics.factorial

class ExampleSuite extends FunSuite {

  test("assert result example") {
    assertResult(120) { factorial(5) }
  }

  test("assert example") {
    assert(1 === factorial(0))
  }

  test("example for throw") {
    assertThrows[Exception] {
      factorial(-1)
    }
  }
}
